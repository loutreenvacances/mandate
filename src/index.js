import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import reportWebVitals from './reportWebVitals';
import Header from "./components/Header"
import Credits from "./components/Credits"
import Party from "./components/Party"

ReactDOM.render(
  <React.StrictMode>
    <Router>
      <Header/>
      <Switch>
      <Route exact path="/" component={App} />
        <Route exact path="/home" component={App} />
        <Route path="/play" component={Party} />   
        <Route path="/credits" component={Credits} />
      </Switch>
    </Router>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
