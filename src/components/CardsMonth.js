import { Button, CardDeck, Card, Container, Row } from "react-bootstrap";


const InfoEffect = ({card, score}) =>
{
    function color(val)
    {
        if(val < 0)
            return "red";
        else if(val === 0)
            return "black";
        else
            return "green";
    }

    function calculAir()
    {
        return ((card.effects.NOx + card.effects.SO2 + card.effects.PM10 + card.effects.PM2_5));
    }

    return <>
    <h6 style={{color: color(calculAir())}}>Qualité de l'air : </h6>
    <ul>
        <li style={{color: color(card.effects.NOx)}}>NOx : {card.effects.NOx}</li>
        <li style={{color: color(card.effects.SO2)}}>SO2 : {card.effects.SO2}</li>
        <li style={{color: color(card.effects.PM10)}}>PM10 : {card.effects.PM10}</li>
        <li style={{color: color(card.effects.PM2_5)}}>PM2.5 : {card.effects.PM2_5}</li>
    </ul>
    <h6 style={{color: color(card.effects.Social)}}>Social : {card.effects.Social}</h6>
    <h6 style={{color: color(card.effects.Economie)}}>Economie : {card.effects.Economie}</h6>
    </>
}

const CardsMonth = ({tabCards, updateParty, score}) =>
{

    
    return <Container>
    <Row>
    <CardDeck>

      <Card style={{ width: '18rem' }}>
          <Card.Img variant="top" src= {"./ImageCard/" + tabCards[0].image}  />
          <Card.Body>
            <Card.Title>Choix 1</Card.Title> 
            <Card.Text>{tabCards[0].text}</Card.Text>              
          </Card.Body>
          <Card.Footer>
                <InfoEffect card={tabCards[0]} score={score}/>
                <Button variant="primary" onClick={() => updateParty(tabCards[0])}>Choisir</Button>
          </Card.Footer>
        </Card>

        <Card style={{ width: '18rem' }}>
          <Card.Img variant="top" src={"./ImageCard/" + tabCards[1].image} />
          <Card.Body>
            <Card.Title>Choix 2</Card.Title>  
            <Card.Text>{tabCards[1].text}</Card.Text>             
          </Card.Body>
          <Card.Footer>
              <InfoEffect card={tabCards[1]} score={score}/>
              <Button variant="primary" onClick={() => updateParty(tabCards[1])}>Choisir</Button>
          </Card.Footer>
        </Card>

        <Card style={{ width: '18rem' }}>
          <Card.Img variant="top" src={"./ImageCard/" + tabCards[2].image} />
          <Card.Body>
            <Card.Title>Choix 3</Card.Title>  
            <Card.Text>{tabCards[2].text}</Card.Text>             
          </Card.Body>
          <Card.Footer>
              <InfoEffect card={tabCards[2]} score={score}/>
              <Button variant="primary" onClick={() => updateParty(tabCards[2])}>Choisir</Button>
          </Card.Footer>
        </Card>

      </CardDeck>
      </Row>
    </Container>
}

export default CardsMonth;