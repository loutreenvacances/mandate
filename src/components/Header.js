import { Navbar, Nav } from "react-bootstrap";
import {LinkContainer} from 'react-router-bootstrap'

const Header = () =>
{
    return <Navbar bg="dark" variant="dark" className="navText">
        <LinkContainer to="/home">
        <img
        src="/ressources/Mandate_Logo.png"
        width="160"
        height="80"
        className="d-inline-block align-top"
        alt="React Bootstrap logo"
      />
        </LinkContainer>
      <Nav className="mr-auto">

        <LinkContainer to="/home">
          <Nav.Link >Explications</Nav.Link>
        </LinkContainer>

        <LinkContainer to="/play">
          <Nav.Link>Jouer</Nav.Link>
        </LinkContainer>

        <LinkContainer to="/credits">
          <Nav.Link>Crédits</Nav.Link>
        </LinkContainer>
      </Nav>
  </Navbar>
}


export default Header;