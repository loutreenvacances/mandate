import React from "react"
import {ProgressBar, Container, Tooltip, OverlayTrigger} from 'react-bootstrap';

const ScoreBoard = ({score}) => {
    
    const createProgressBar = (categorie, max) => {
        let color;
        if(categorie<0.2*max)
        {
            color = "danger";
        }
        else if(categorie<0.5*max)
        {
            color = "warning";
        }
        else
        {
            color = "success";
        }

        if(categorie > max){
            categorie=max;
        }
        else if( categorie < 0){
            categorie =0;
        }
        return <ProgressBar now={categorie} label={`${categorie}/${max}`}variant={color} max={max} min={0}/>
    }
    
        return <Container>   
            <OverlayTrigger
                placement="right"
                delay={{ show: 250, hide: 400 }}
                overlay={
                    <Tooltip id="button-tooltip">
                      Qualité de l'air dans la ville, tous les 2 tours (semestres) vous regagnez 2 pour chaque capacité de polluant. En effet au bout d'un moment les polluants sont absorbés par la nature.
                    </Tooltip>}
            >
                <h6>Qualité de l'air : </h6>
            </OverlayTrigger>
            <ul>
                <OverlayTrigger
                    placement="right"
                    delay={{ show: 250, hide: 400 }}
                    overlay={
                        <Tooltip id="button-tooltip">
                        Oxyde d'azote produit en majorité par la combustion de carburant fossile
                        </Tooltip>}
                >
                    <p>NOx {createProgressBar(score.NOx, 100)}</p>
                </OverlayTrigger>

                <OverlayTrigger
                    placement="right"
                    delay={{ show: 250, hide: 400 }}
                    overlay={
                        <Tooltip id="button-tooltip">
                        Dioxyde de souffre, produit par des procédés industriels, éruptions volcanique et la combustion de certaines énergies fossiles non-désulfurées
                        </Tooltip>}
                >
                    <p>SO2 {createProgressBar(score.SO2, 100)}</p>
                </OverlayTrigger>

                <OverlayTrigger
                    placement="right"
                    delay={{ show: 250, hide: 400 }}
                    overlay={
                        <Tooltip id="button-tooltip">
                            Micro-particules de taille indérieur à 10 microns ( aérosol, poussière, cendre, fumée tabac ... ).
                        </Tooltip>}
                >
                    <p>PM10 {createProgressBar(score.PM10, 100)}</p>
                </OverlayTrigger>

                <OverlayTrigger
                    placement="right"
                    delay={{ show: 250, hide: 400 }}
                    overlay={
                        <Tooltip id="button-tooltip">
                            Micro-particules de taille indérieur à 2,5 microns ( brouillard industriel, poussière d'argile, particules diesel, métaux lourds ... ). Les PM2,5 sont aussi des PM10
                        </Tooltip>}
                >
                    <p>PM2,5 {createProgressBar(score.PM2_5, 100)}</p>
                </OverlayTrigger>
            </ul>

            <OverlayTrigger
                    placement="right"
                    delay={{ show: 250, hide: 400 }}
                    overlay={
                        <Tooltip id="button-tooltip">
                            La santé de vos habitants est impactée par le qualité de l'air. Si votre santé est trop basse, votre Social et votre Economie risque de diminué
                        </Tooltip>}
                >
                    <h6>Santé {createProgressBar(score.Sante, 30)}</h6>
                </OverlayTrigger>
            
            <OverlayTrigger
                    placement="right"
                    delay={{ show: 250, hide: 400 }}
                    overlay={
                        <Tooltip id="button-tooltip">
                           Joie de vivre de vos habitants, est impacté par les activités de votre ville et la simplicité à y vivre
                        </Tooltip>}
            >
                <h6>Social {createProgressBar(score.Social, 100)}</h6>
            </OverlayTrigger>

            <OverlayTrigger
                    placement="right"
                    delay={{ show: 250, hide: 400 }}
                    overlay={
                        <Tooltip id="button-tooltip">
                           Argent restant dans les caisses de la ville
                        </Tooltip>}
            >
                <h6>Économie {createProgressBar(score.Economie, 100)}</h6>
            </OverlayTrigger>
        </Container>;
}
export default ScoreBoard;