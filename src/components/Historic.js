import { Button, Table, Container, Row, Col} from "react-bootstrap";
import { useState } from 'react';
const Historic = ({historic}) =>
{
    const createTable = (card) =>
    {
        
        return <Table striped bordered hover variant="dark">
            <thead>
                <tr>
                <th>Nox</th>
                <th>SO2</th>
                <th>PM10</th>
                <th>PM2.5</th>
                <th>Economie</th>
                <th>Social</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                <td>{card.effects.NOx}</td>
                <td>{card.effects.SO2}</td>
                <td>{card.effects.PM10}</td>
                <td>{card.effects.PM2_5}</td>
                <td>{card.effects.Economie}</td>
                <td>{card.effects.Social}</td>
                </tr>
            </tbody>
        </Table>
    }

    const [showHisto, setShowHisto] = useState(false);

    if(!showHisto)
    {
        return <>
        <Button variant="primary" onClick={() => setShowHisto(true)}>
            Afficher historique
        </Button>

    </>
    }
    else
    {
        if(historic.length === 0)
        {
            return <>
            <Button variant="primary" onClick={() => setShowHisto(false)}>
                Cacher historique
            </Button>
            </>
        }
        else if (historic.length === 1)
        {
            return <>
        <Button variant="primary" onClick={() => setShowHisto(false)}>
            Cacher historique
        </Button>
        <Container>
        <Row>
            <Col><h6>Choix il y a 1 tour :</h6> {createTable(historic[0])}</Col>           
        </Row>
        </Container>
        </>
        }
        else if (historic.length === 2)
        {
            return <>
        <Button variant="primary" onClick={() => setShowHisto(false)}>
            Cacher historique
        </Button>
        <Container>
        <Row>
            <Col><h6>Choix il y a 1 tour :</h6> {createTable(historic[0])}</Col>         
        </Row>
        <Row>
            <Col><h6>Choix il y a 2 tours :</h6>{createTable(historic[1])}</Col>
        </Row>
        </Container>
        </>
        }
        else
        {
            return <>
        <Button variant="primary" onClick={() => setShowHisto(false)}>
            Cacher historique
        </Button>
        <Container>
        <Row>
            <Col><h6>Choix il y a 1 tour :</h6> {createTable(historic[0])}</Col>         
        </Row>
        <Row>
            <Col><h6>Choix il y a 2 tours :</h6>{createTable(historic[1])}</Col>
            <Col><h6>Choix il y a 3 tours :</h6>{createTable(historic[2])}</Col>
        </Row>
        </Container>
        </>
        }
    }   
}

export default Historic;