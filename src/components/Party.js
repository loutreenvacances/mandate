import { useEffect, useState } from 'react';
import PickPlayer from "./PickPlayer"
import CardsMounth from "./CardsMonth"
import JsonData from "../data.json"
import ScoreBoard from "./ScoreBord"
import Tuto from './Tuto'
import Historic from './Historic'
import { Col, Container, Row, Card, Modal, Button, Image, Alert} from "react-bootstrap";
import {LinkContainer} from 'react-router-bootstrap'


const InitParty = () => {
    let [characterSelected, setCharacterSelected] = useState(null);
    let [tutoFinish, setTutoFinish] = useState(false);

    const selectCharacter = (character) => {
        setCharacterSelected(character);
    }
    return <>
        { characterSelected == null ? <PickPlayer selectCharacter={selectCharacter} /> : tutoFinish === false ? <Tuto pickPerso={characterSelected} setTutoFinish={setTutoFinish} /> : <Party characterSelected={characterSelected} />}
    </>;
}

const Victory = () =>
{
    return <Alert variant="success">
    <Alert.Heading>Bravo monsieur le maire !</Alert.Heading>
    <p>
        Après 3 longs mandats vous avez réussi à maintenir la ville dans de bonnes conditions. Vos habitants se souviendront de vous comme une personne juste, doué en finance et qui ne les a jamais délaissé.  
    </p>
    <hr />
        <div className="d-flex justify-content-end">
        <LinkContainer to="/home">
          <Button variant="outline-success">
            Retour à l'accueil
          </Button>
          </LinkContainer>
        </div>
    </Alert>
}

const Party = ({ characterSelected }) => {
    let [currentSemester, setCurrentSemester] = useState(-1);
    let [currentMandate, setCurrentMandate] = useState(1);
    let [cards, setCards] = useState([]);
    let [cardsReady, setCardsReady] = useState(false);
    let [partyStatus, setPartyStatus] = useState(null);
    let [score, setScore] = useState({ Social: 50, Economie: 50, NOx: 50, SO2: 50, PM10: 50, PM2_5: 50, Sante:30 });
    let [historic, setHistoric] = useState([]);
    let [victory, setVictory] = useState(false);

    const updateHisto = (cardChosen) =>
    {
        if(historic.length === 0)
            setHistoric([cardChosen]);
        else if(historic.length === 1)
            setHistoric(oldHisto => [cardChosen, oldHisto[0]]);
        else 
            setHistoric(oldHisto => [cardChosen, oldHisto[0], oldHisto[1]]);
    }

    const updateParty = (cardChosen) => {
        setCardsReady(false);

        updateHisto(cardChosen);
        let pollutionDown = 0;

        if(currentSemester%2===0 && !(currentMandate===1 && currentSemester===0)){
            pollutionDown =2;
        }
        //Mise a jour du score du joueur
        setScore({
            Social: valeurPlafondSol(score.Social + cardChosen.effects.Social, 100, 0),
            Economie: valeurPlafondSol(score.Economie + cardChosen.effects.Economie, 100, 0),
            NOx: valeurPlafondSol(score.NOx + cardChosen.effects.NOx + pollutionDown, 100, 0),
            SO2: valeurPlafondSol(score.SO2 + cardChosen.effects.SO2 + pollutionDown, 100, 0),
            PM10: valeurPlafondSol(score.PM10 + cardChosen.effects.PM10 + pollutionDown, 100, 0),
            PM2_5: valeurPlafondSol(score.PM2_5 + cardChosen.effects.PM2_5 + pollutionDown, 100, 0),
            Sante: valeurPlafondSol(score.Sante + calculModifierScoreSante(), 30, 0)
        });
        setScore(oldScore => ({...oldScore,
            Social: valeurPlafondSol(oldScore.Social + setEcoSocial(), 100, 0),
            Economie: valeurPlafondSol(oldScore.Economie + setEcoSocial(), 100, 0)
        }));
        
       
        
        
        if(currentMandate === 3 && currentSemester === 12 && partyStatus === null)
        {
            setVictory(true);
        }


    }

    const valeurPlafondSol= (valeur, max, min) =>{
        if(valeur>max){
            valeur = max;
        }
        if(valeur<min){
            valeur = min;
        }
        return valeur
    }



    const calculModifierScoreSante= () =>{
        let modifierSante=0;
        //__________NOx__________
        if(score.NOx<20 ){
            modifierSante-=2;
        }
        else if(score.NOx<50 ){
            modifierSante-=1;
        }
        else if(score.NOx<70){
            modifierSante+=0;
        }
        else{
            modifierSante+=1
        }
        //_________SO2____________
        if(score.SO2<20){
            modifierSante-=2;
        }
        else if(score.SO2<50 ){
            modifierSante-=1;
        }
        else if(score.SO2<70){
            modifierSante+=0;
        }
        else{
            modifierSante+=1
        }
        //_________PM10_____________
        if(score.PM10<20){
            modifierSante-=2;
        }
        else if(score.PM10<50 ){
            modifierSante-=1;
        }
        else if(score.PM10<70){
            modifierSante+=0;
        }
        else{
            modifierSante+=1
        }
        //_________PM2.5_____________
        if(score.PM2_5<20){
            modifierSante-=2;
        }
        else if(score.PM2_5<50 ){
            modifierSante-=1;
        }
        else if(score.PM2_5<70){
            modifierSante+=0;
        }
        else{
            modifierSante+=1
        }
        
       return modifierSante;
    }

    
    const setEcoSocial = () => {
        let modificateur = 0;
        if(score.Sante <= 5)
        {
            modificateur = -10;
        }
        else if (score.Sante <= 10)
        {
            modificateur = -5;
        }
        else if (score.Sante <= 20)
        {
            modificateur = -2;
        }
        return modificateur;
    }

    const verifyPartyStatus = () => {
        let deathCause = null;
        for (const [bar, value] of Object.entries(score)) {
            if (bar === "Economie" || bar === "Social" || bar === "Sante"){
                if (value <= 0)
                    deathCause = bar;
            }
        }
        
        if(deathCause === null && currentSemester === 12)
        {
            if(score.Economie < 50)
                deathCause = "MandatEconomie";
            else if(score.Social < 50) 
                deathCause = "MandatSocial";
            else
            {
                setCurrentSemester(-1);
                setCurrentMandate(oldMandate => oldMandate + 1);
            }
        }

        return deathCause;
    }

    const pickRandomCards = () => {
        return new Promise((successCallback, failureCallback) => {
            let cardsArray = [];

            for (let i = 0; i < 3;) {
                let card = JsonData.GameCards[Math.floor(Math.random() * (JsonData.GameCards.length))];

                if (!cardsArray.includes(card)) {
                    cardsArray[i] = card;
                    i++;
                }
            }

            if (cardsArray.length !== 0) {
                successCallback(cardsArray);
            } else {
                failureCallback("echec");
            }
        })
    }

    //Appellé quand le score change => verification du status de la partie + changement de mois
    useEffect(() => {
        let status = verifyPartyStatus();
        
        if (status == null) {
            setCurrentSemester(oldMounth => oldMounth + 1);
        }
        else {
            setPartyStatus(status);
        }
    }, [score]);

    //Appellé quand le mois change => on retire 3 nouvelles cartes
    useEffect(() => {
        pickRandomCards()
            .then(cards => {

                setCards(cards);
                setCardsReady(true);
            })
            .catch()
    }, [currentSemester]);

    return <>
        { partyStatus != null ? <AlertPartyOver partyStatus={partyStatus} /> : '' }
        <br></br>
        <Container fluid>
            <Row>
                <Col xs={3}>
                    <Card>
                        <Card.Body>
                            <Card.Title>Scores : </Card.Title>
                            <ScoreBoard score={score} />
                        </Card.Body>
                        <Card.Footer>
                            <h5>Nombre de semestres passés : {currentSemester}</h5>
                            <h5>Numéro du mandat : {currentMandate}</h5>
                            <Image src={"./ressources/"+characterSelected+".png"} thumbnail className='imageJoueur' />
                        </Card.Footer>
                    </Card>
                </Col>
                {!victory || partyStatus !== null ? <Col xs={9}>{cardsReady ? <CardsMounth tabCards={cards} updateParty={updateParty} score={score} /> : ''}</Col> : <Col xs={9}><Victory/></Col>}               
            </Row>
            <Row>
                <Col>
                {<Historic historic={historic}/>}
                </Col>
            </Row>
        </Container>
    </>;

}

const AlertPartyOver = ({partyStatus}) => {
    let [partyOverTitle, setPartyOverTitle] = useState("");
    let [partyOverText, setPartyOverText] = useState("");

    useEffect(() => {
        let go = JsonData.GameOver.filter(go => go.id === partyStatus)[0];
        setPartyOverTitle(go.title);
        setPartyOverText(go.text);
    }, [partyStatus])

    return (
        <>
            <Modal
                show={true}
                backdrop="static"
                keyboard={false}
            >
                <Modal.Header>
                    <Modal.Title>{partyOverTitle}</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    {partyOverText}
                </Modal.Body>
                <Modal.Footer>
                    <LinkContainer to="/home">
                        <Button variant="primary" >Retourner à l'accueil</Button>
                    </LinkContainer>
                </Modal.Footer>
            </Modal>
        </>
    );
}

export default InitParty;