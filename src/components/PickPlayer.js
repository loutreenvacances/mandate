import { Button, CardDeck, Card, Container, Row } from "react-bootstrap";

const PickPlayer = ({selectCharacter}) =>
{
    return <Container>
    <Row>
    <CardDeck>
      <Card style={{ width: '18rem' }}>
          <Card.Img variant="top" src="./ressources/classique.png" />
          <Card.Body>
            <Card.Title>C'est la StartUp Nation</Card.Title>               
          </Card.Body>
          <Card.Footer><Button variant="primary" onClick={() => selectCharacter("classique")}>Choisir</Button></Card.Footer>
        </Card>

        <Card style={{ width: '18rem' }}>
          <Card.Img variant="top" src="./ressources/hippie.png" />
          <Card.Body>
            <Card.Title>J'suis où là ?</Card.Title>               
          </Card.Body>
          <Card.Footer><Button variant="primary"  onClick={() => selectCharacter("hippie")}>Choisir</Button></Card.Footer>
        </Card>

        <Card style={{ width: '18rem' }}>
          <Card.Img variant="top" src="./ressources/patron.png" />
          <Card.Body>
            <Card.Title>Buisiness is Buisiness</Card.Title>               
          </Card.Body>
          <Card.Footer><Button variant="primary" onClick={() => selectCharacter("patron")}>Choisir</Button></Card.Footer>
        </Card>

        <Card style={{ width: '18rem' }}>
          <Card.Img variant="top" src="./ressources/vieux.png" />
          <Card.Body>
            <Card.Title>Investissez dans les bananes !</Card.Title>               
          </Card.Body>
          <Card.Footer><Button variant="primary" onClick={() => selectCharacter("vieux")}>Choisir</Button></Card.Footer>
        </Card>
      </CardDeck>
      </Row>
    </Container>
}

export default PickPlayer;