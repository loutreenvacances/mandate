import { CardDeck, Card, Container, Row } from "react-bootstrap";

const Credits = () =>
{
    return <>
        <Container>
        <h1>Credits :</h1>
            <Row>
            <CardDeck>
                <Card style={{ width: '18rem' }}>
                <Card.Img variant="top" src="./ressources/credits/baron.png" />
                <Card.Body>
                    <Card.Title>DUBOCAGE Julien</Card.Title>       
                    <Card.Text>
                        <ul>
                            <li>Programmeur</li>
                            <li>Propriétaire d'un Bescherelle</li>
                        </ul>
                    </Card.Text>        
                </Card.Body>
                </Card>

                <Card style={{ width: '18rem' }}>
                <Card.Img variant="top" src="./ressources/credits/lezard.png" />
                <Card.Body>
                    <Card.Title>SATTLER Emma</Card.Title> 
                    <Card.Text>
                        <ul>
                            <li>Programmeur</li>
                            <li>Excel Master</li>
                        </ul>
                    </Card.Text>                 
                </Card.Body>
                </Card>

                <Card style={{ width: '18rem' }}>
                <Card.Img variant="top" src="./ressources/credits/knight.png" />
                <Card.Body>
                    <Card.Title>METZ Florian</Card.Title>  
                    <Card.Text>
                        <ul>
                            <li>Programmeur</li>
                            <li>Écrivain en herbe</li>
                        </ul>
                    </Card.Text>                
                </Card.Body>
                </Card>

                <Card style={{ width: '18rem' }}>
                <Card.Img variant="top" src="./ressources/credits/boureau.png" />
                <Card.Body>
                    <Card.Title>FRANCOIS Michael</Card.Title>               
                    <Card.Text>
                        <ul>
                            <li>Programmeur</li>
                            <li>Picasso du photoshop</li>
                        </ul>
                    </Card.Text>   
                </Card.Body>
                </Card>
            </CardDeck>
            </Row>
        </Container>
    </>
}



export default Credits;