import { Button, CardDeck, Card, Container, Row, Col } from "react-bootstrap";
import data from "../data.json";
import { useState } from 'react';
import CardsMonth from "./CardsMonth";
import ScoreBoard from "./ScoreBord"

const perso = ["classique", "hippie", "patron", "vieux"];
const Part1 = ({nonPick, setNumPart, setTutoFinish}) =>
{
    const tabCards = [data.TutoCards[0], data.TutoCards[1], data.TutoCards[2]];
    return <Container>
    <Row>
    <CardDeck>

      <Card style={{ width: '18rem' }}>
          <Card.Img variant="top" src= {"./ressources/" + nonPick[0] + ".png"}  />
          <Card.Body>
            <Card.Title>Conseiller 1</Card.Title> 
            <Card.Text>{tabCards[0].text}</Card.Text>              
          </Card.Body>
          <Card.Footer>
          </Card.Footer>
        </Card>

        <Card style={{ width: '18rem' }}>
          <Card.Img variant="top" src={"./ressources/" + nonPick[1] + ".png"} />
          <Card.Body>
            <Card.Title>Conseiller 2</Card.Title>  
            <Card.Text>{tabCards[1].text}</Card.Text>             
          </Card.Body>
          <Card.Footer>
          </Card.Footer>
        </Card>

        <Card style={{ width: '18rem' }}>
          <Card.Img variant="top" src={"./ressources/" + nonPick[2] + ".png"} />
          <Card.Body>
            <Card.Title>Conseiller 3</Card.Title>  
            <Card.Text>{tabCards[2].text}</Card.Text>             
          </Card.Body>
          <Card.Footer>
          </Card.Footer>
        </Card>
      </CardDeck>
      </Row>
      <Row>
      <Button onClick={() => setNumPart(n => 2)}>Suite</Button>
      <Button onClick={() => setTutoFinish(true)}>Passer le tuto</Button>
      </Row>
    </Container>
}

const Part2 = ({nonPick, setNumPart}) =>
{
    const tabCards = [data.TutoCards[3], data.TutoCards[4], data.TutoCards[5]];
    return <Container>
    <Row>
    <CardDeck>

      <Card style={{ width: '18rem' }}>
          <Card.Img variant="top" src= {"./ressources/" + nonPick[0] + ".png"}  />
          <Card.Body>
            <Card.Title>Conseiller 1</Card.Title> 
            <Card.Text>{tabCards[0].text}</Card.Text>              
          </Card.Body>
          <Card.Footer>
          </Card.Footer>
        </Card>

        <Card style={{ width: '18rem' }}>
          <Card.Img variant="top" src={"./ressources/" + nonPick[1] + ".png"} />
          <Card.Body>
            <Card.Title>Conseiller 2</Card.Title>  
            <Card.Text>{tabCards[1].text}</Card.Text>             
          </Card.Body>
          <Card.Footer>
          </Card.Footer>
        </Card>

        <Card style={{ width: '18rem' }}>
          <Card.Img variant="top" src={"./ressources/" + nonPick[2] + ".png"} />
          <Card.Body>
            <Card.Title>Conseiller 3</Card.Title>  
            <Card.Text>{tabCards[2].text}</Card.Text>             
          </Card.Body>
          <Card.Footer>
          </Card.Footer>
        </Card>

      </CardDeck>
      </Row>
      <Row>
      <Button onClick={() => setNumPart(1)}>Retour</Button>
      <Button onClick={() => setNumPart(3)}>Suite</Button>
      </Row>
    </Container>
}

const Part3 = ({ setNumPart, score, setScore}) =>
{
    const tabCards = [data.TutoCards[6], data.TutoCards[7], data.TutoCards[8]];
    
    
    const updateParty = (cardChosen) => {

      //Mise a jour du score du joueur
      setScore({Social : score.Social + cardChosen.effects.Social,
          Economie : score.Economie + cardChosen.effects.Economie,
          NOx : score.NOx + cardChosen.effects.NOx,
          SO2 : score.SO2 + cardChosen.effects.SO2,
          PM10 : score.PM10 + cardChosen.effects.PM10,
          PM2_5 : score.PM2_5 + cardChosen.effects.PM2_5,
          Sante : score.Sante,
          //Correspond a l'addition des score des polluants (c'est a modifier car c'est très idiot)
      });
      setNumPart(4);
  }


    return <>
    <br></br>
    <Container fluid>
    <Row>
        <Col xs={3}>
            <Card>            
                <Card.Body> 
                <   Card.Title>Scores : </Card.Title>
                    <ScoreBoard score={score} /> 
                </Card.Body>
            </Card>
        </Col>
        <Col xs={9}>{ <CardsMonth tabCards={tabCards} updateParty={updateParty} score={score}/> }</Col>
    </Row>
    <Row>
      <Button onClick={() => setNumPart(2)}>Retour</Button>
    </Row>
    </Container>
    </>;
}

const Part4 = ({nonPick, setNumPart, setTutoFinish, score}) =>
{
    const tabCards = [data.TutoCards[9], data.TutoCards[10], data.TutoCards[11]];

    return <Container fluid>
    <Row>
    <Col xs={3}>
            <Card>            
                <Card.Body> 
                <   Card.Title>Scores : </Card.Title>
                    <ScoreBoard score={score} /> 
                </Card.Body>
            </Card>
        </Col>
      <Col xs={9}>
        <CardDeck>

          <Card style={{ width: '18rem' }}>
              <Card.Img variant="top" src= {"./ressources/" + nonPick[0] + ".png"}  />
              <Card.Body>
                <Card.Title>Conseiller 1</Card.Title> 
                <Card.Text>{tabCards[0].text}</Card.Text>              
              </Card.Body>
              <Card.Footer>
              </Card.Footer>
            </Card>

            <Card style={{ width: '18rem' }}>
              <Card.Img variant="top" src={"./ressources/" + nonPick[1] + ".png"} />
              <Card.Body>
                <Card.Title>Conseiller 2</Card.Title>  
                <Card.Text>{tabCards[1].text}</Card.Text>             
              </Card.Body>
              <Card.Footer>
              </Card.Footer>
            </Card>

            <Card style={{ width: '18rem' }}>
              <Card.Img variant="top" src={"./ressources/" + nonPick[2] + ".png"} />
              <Card.Body>
                <Card.Title>Conseiller 3</Card.Title>  
                <Card.Text>{tabCards[2].text}</Card.Text>             
              </Card.Body>
              <Card.Footer>
              </Card.Footer>
            </Card>

          </CardDeck>
      </Col>
      </Row>
      <Row>
      <Button onClick={() => setNumPart(3)}>Retour</Button>
      <Button onClick={() => setTutoFinish(true)}>Commencer !</Button>
      </Row>
    </Container>
}

const Tuto = ({pickPerso, updateParty, setTutoFinish}) =>
{
    const [numPart, setNumPart] = useState(1);
    const nonPickPerso = perso.filter(p => p !== pickPerso);
    let [score, setScore] = useState({Social:50, Economie:50, NOx:50, SO2:50, PM10:50, PM2_5:50, Sante:30});
    return <>
    {numPart === 1 ? <Part1 nonPick={nonPickPerso} setNumPart={setNumPart} setTutoFinish={setTutoFinish}/> : numPart === 2 ? <Part2 nonPick={nonPickPerso} setNumPart={setNumPart}/> : numPart === 3 ? <Part3 setNumPart={setNumPart} updateParty={updateParty} score={score} setScore={setScore}/> : <Part4 nonPick={nonPickPerso} setNumPart={setNumPart} setTutoFinish={setTutoFinish} score={score}/>}
    </>
}

export default Tuto;