import 'bootstrap/dist/css/bootstrap.min.css';
import './style.css';
import { Jumbotron, Container, Carousel, Button } from 'react-bootstrap';
import {LinkContainer} from 'react-router-bootstrap'

const App = () =>
{
  return <>
      <Jumbotron>
          <h1>Bienvenue sur Mandate !</h1>
          <p>Dans ce jeux de gestion pas du tout inspiré de Reigns, vous allez devenir le Maire d'une ville.</p>
          <p>Votre objectif sera que vos habitants soient heureux, tout en gardant une économie correcte et en limitant les quantités de polluants.</p>
          <p>Quand vous aurez cliqué sur Jouer ci-dessus vous serez d'abord confrontés à un petit tutoriel qui vous expliquera les bases du jeu</p>
          <p>En attendant que vous vous lanciez dans l'aventure voici déjà plus bas quelques images du jeu.</p>

          <p>
          <LinkContainer to="/play">
            <Button variant="primary">Jouer !</Button>
          </LinkContainer>
          </p>
      </Jumbotron>

      <Container>
        <Carousel>
          <Carousel.Item>
            <img
              className="d-block w-100"
              src="ImagesHomeReadMe/ChoixPersonnage.png"
              alt="First slide"
            />
          </Carousel.Item>
          <Carousel.Item>
            <img
              className="d-block w-100"
              src="ImagesHomeReadMe/ChoixProjet.png"
              alt="Second slide"
            />
          </Carousel.Item>
          <Carousel.Item>
            <img
              className="d-block w-100"
              src="ImagesHomeReadMe/Tuto.png"
              alt="Third slide"
            />
          </Carousel.Item>

          <Carousel.Item>
            <img
              className="d-block w-100"
              src="ImagesHomeReadMe/ProjetsWithHisto.png"
              alt="Thourth slide"
            />
          </Carousel.Item>
          <Carousel.Item>
            <img
              className="d-block w-100"
              src="ImagesHomeReadMe/ExplicationBarre.png"
              alt="Thourth slide"
            />
          </Carousel.Item>
        </Carousel>
      </Container>
  </>
   
}

export default App;
