# Utilisation de l'image Node.js version 16.16 en tant qu'image de base
FROM node:16.16-alpine as build

# Création du répertoire de travail de l'application dans le conteneur
WORKDIR /usr/src/app

# Copie des fichiers package.json et package-lock.json dans le répertoire de travail
COPY package*.json ./

# Installation des dépendances de l'application
RUN npm install

# Copie du reste des fichiers de l'application dans le répertoire de travail
COPY . .

# Build de l'application React
RUN npm run build

# Utilisation d'une image légère Nginx pour servir l'application construite
FROM nginx:alpine

# Copie des fichiers de build depuis l'étape précédente
COPY --from=build /usr/src/app/build /usr/share/nginx/html

# Exposition du port 80 pour accéder à l'application
EXPOSE 80

# Commande par défaut pour démarrer Nginx et servir l'application
CMD ["nginx", "-g", "daemon off;"]
