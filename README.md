![Logo Mandate](./public/ressources/Mandate_Logo.png)

## Notre Projet

Nous avons choisi de faire un jeu de gestion en s'inspirant du sytème de [Reigns](https://store.steampowered.com/app/474750/Reigns/?l=french) ("jeu de cartes" ou doit effectuer des des décisions qui aurant un impact sur la partie). 

Le joueur arrive sur la page d'acceuil de notre jeu, lui expliquant rapidement le principe et l'objectif et l'invitant à lancer une partie.

![Acceuil](./public/ImagesHomeReadMe/Accueil.png)

Une fois le jeu lancé le joueur devra commencer par choisir le personnage qu'il veut incarner (uniquement esthétique pour ce prototype mais on peux imaginer différent set des cartes en fonction du personnage choisi plus tard)

![ChoixPersonnage](./public/ImagesHomeReadMe/ChoixPersonnage.png)

Après le choix du personnage, les 3 personnages non-sélectionnés expliquent le jeu au joueur, ce qui fait office de tutoriel.

![tuto](./public/ImagesHomeReadMe/Tuto.png)

A la fin du tuto la partie commence et le joueur commencera à faire ses choix de projets.

Chaque projet est représenté par une carte et son énoncé, chacunes de ces cartes a des valeurs spécifiques. 
Ces valeurs vont modifier des "barres de score" en positif ou en négatif. Ces barres représentent l'économie, le social et la qualité de l'air de la ville (représentée par plusieurs sous-barres de gaz et particules qui affectent la barre de Santé).

Voici quelques captures d'écran du jeu :

![ChoixProjet](./public/ImagesHomeReadMe/ChoixProjet.png)

Si vous décidez d'afficher l'historique voilà à quoi il ressemble : 

![Historique](./public/ImagesHomeReadMe/ProjetsWithHisto.png)

## Procédure d'installation et d'éxecution

Pour accéder a notre jeu directement hébergé en ligne sur un serveur personnel cliquez ici : 

[Mandate ! ](https://mandate.mikl.fr)

Pour lancer le jeu en local à partir des fichiers sources (nécessite une installation de NodeJS fonctionelle en 16.16.0) :

```
1) Faire un clone du projet : 
    git clone https://gitlab.com/loutreenvacances/mandate.git
2) Installer les modules nécessaires au jeu dans le dossier :
    npm install
    npm install react-bootstrap bootstrap
    npm install react-router-bootstrap
    npm install react-router-dom
3) Lancer le serveur React via la commande
    npm start
```

